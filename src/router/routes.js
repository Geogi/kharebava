// @import: Route views
const HomePage = () => import(/* webpackChunkName: "home" */ "@/views/Home");
const AboutPage = () => import(/* webpackChunkName: "about" */ "@/views/About");
const PageNotFound = () => import(/* webpackChunkName: "404" */ "@/views/404");

// @defie & export: Routes array
export default [
  {
    path: "/",
    name: "home",
    component: HomePage
  },
  {
    path: "/about",
    name: "about",
    component: AboutPage
  },
  {
    path: "*",
    name: "pagenotfound",
    component: PageNotFound
  }
];
