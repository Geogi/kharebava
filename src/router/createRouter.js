// @import: Global modules
import Vue from "vue";
import VueRouter from "vue-router";

// @import: Routes
import routes from "./routes";

// @do: Register vue router plugin
Vue.use(VueRouter);

// @define & export: New router instance
export default () =>
  new VueRouter({
    mode: "history",
    routes
  });
