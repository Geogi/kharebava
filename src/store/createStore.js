// @import: Global modules
import Vue from "vue";
import Vuex from "vuex";

// @do: Register vuex plug
Vue.use(Vuex);

export default () =>
  new Vuex.Store({
    modules: {}
  });
