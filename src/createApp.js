// @import: Global modules
import Vue from "vue";
import VueMeta from "vue-meta";

// @import: Components
import App from "./App.vue";

// @import: Vue router and store creator functions
import createRouter from "./router/createRouter";
import createStore from "./store/createStore";

// @do: Register vue plugins
Vue.use(VueMeta, {
  keyName: "metaInfo",
  attribute: "data-vue-meta",
  ssrAttribute: "data-vue-meta-server-rendered",
  tagIDKeyName: "vmid"
});

// @export: createApp function
export default () => {
  // @define: Router & Store
  const router = createRouter();
  const store = createStore();

  // @define: Main vue instance as app
  const app = new Vue({
    store,
    router,
    render: h => h(App)
  });

  // @return: App data
  return {
    app,
    router,
    store
  };
};
